import React, { Component } from "react";
import Header from './components/Header';
import SideMenu from './components/SideMenu';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="app-container">
          <div className="left-side-section">
            <SideMenu />
          </div>
          <div className="main-section">
            <Header />
          </div>
        </div>
      </div>
    );
  }
};



export default App;
