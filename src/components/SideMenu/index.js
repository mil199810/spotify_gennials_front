import React from "react";
import "./SideMenu.css";

const SideMenu = () => {
  return (
    <ul className="side-menu-container">
      <h3 className="user-library-header">Bienvenido a Spotify Gennials</h3>
      <li className="active side-menu-item">
        Albums
      </li>
    </ul>
  );
};

export default SideMenu;