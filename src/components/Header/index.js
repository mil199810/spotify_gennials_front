import React, { Component } from "react";
import axios from 'axios'
import "./Header.css";

class Header extends Component {
  state = {
    searchTerm: "",
    list: [],
  };

  updateSearchTerm = e => {
    this.setState({
      searchTerm: e.target.value
    });
  };

  getListadoAlbums = (searchTerm) => {
    const url = '/list_albums/' + searchTerm
    axios.get(url)
      .then((response) => {
        console.log(searchTerm);
        console.log(response.data);
        this.setState({ list: response.data.albums.items })
      })
      .catch((error) => {
        console.log('error', error);
      })
  };

  render() {
    const { state: { list }, getListadoAlbums } = this
    return (
      <>
        <div className="header">
          <div className="track-search-container">
            <form >
              <input onChange={this.updateSearchTerm} type="text" placeholder="Albúmes..." />
              <button onClick={e => {e.preventDefault(); getListadoAlbums(this.state.searchTerm); }}>
                <i className="fa fa-search search" aria-hidden="true" />
              </button>
            </form>
          </div>
        </div>
        <div className="main-section-container">
          <hr></hr>
          <div>
            <h3 className='header-title'>{this.state.list.length} resultados de álbumes para "{this.state.searchTerm}" </h3>
          </div>
          <ul className="album-view-container">
            {
              list.map((elem, idx) => (
                <li className="album-item" key={idx}>
                  <div>
                    <div className="album-image">
                      <img alt="album" src={elem.images[0].url} />
                      <div className="play-song">
                        <i className="fa fa-play-circle-o play-btn" aria-hidden="true" />
                      </div>
                    </div>
                    <div className="album-details">
                      <p className="album-name">{elem.name}</p>
                      <p className="artist-name">{elem.artists[0].name}</p>
                    </div>
                  </div>
                </li>
              ))
            }
          </ul>
        </div>
      </>
    );
  }
}


export default Header;
